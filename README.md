# 04.02.Mineria_de_texto

* Recuperación y Minería de Texto - Miércoles de 18 a 21 hs.
* https://exactas-uba.zoom.us/j/83741659033
* ID de reunión: 837 4165 9033
* Código de acceso: rmt22-rsm

El primer día de clases podrán ver el curso en el campus.

Por último, les informamos que las clases presenciales de la materia serán los siguientes días:

Miércoles (18 a 21hs)
- 24 de Agosto

Sábados (10 a 13hs)
- 10 de Septiembre
- 1 de Octubre
- 12 de Noviembre
- 19 de Noviembre

Finalización del año:
* Miércoles 9/11: Tareas de NLP + Consultas TP
* Sábado 12/11: Primera tanda de presentaciones de TP [Presencial]
* Miércoles 16/11: Segunda tanda de presentaciones de TP [Presencial]
* Sábado 19/11: Clase suspendida. Todes invitades al workshop de NLP*
* Miércoles 23/11: Repaso pre-parcial
* Miércoles 30/11: Parcial [A confirmar]

## Profesores

* Bruno Bianchi (bbianchi@dc.uba.ar)
* Edgar Altszyler (ealtszyler@dc.uba.ar)

Mail de la materia: textminingexactas@gmail.com

## Modalidad

* Clases teorico-practicas
* Exámen teórico: TBA
* Proyecto final:
    * En grupos de 3 o 4 estudiantes
    * Elegir una tarea clara y concisa
    * Corpus
    * Objetivo claro
    * Hipotesis a testear
    * Tarea a realizar (simulando un trabajo freelance)
    * Presentación oral (una al inicio, una al final)

## links

* Campus - text mining: 
  * http://157.92.26.246/campus/course/view.php?id=13
* Campus - text mining - avisos
  * http://157.92.26.246/campus/mod/forum/view.php?id=303

