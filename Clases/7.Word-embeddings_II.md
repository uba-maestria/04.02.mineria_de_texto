# Word-embeddings II

**[Word-embeddings II](http://157.92.26.246/campus/course/view.php?id=13#section-7)**

## **Contenido**

- Word2vec
- Glove
- Evaluación de los métodos
- Alcances y Limitaciones
- Sesgos
- Visualización de palabras (MDS y T-SNE)

## **Práctica**

- paráctica embeddings [[LINK](https://colab.research.google.com/drive/1Zrg2nbU-LlqD74177vci25TRLu0eHTF9?usp=sharing)]
- práctica TF-IDF [[LINK](https://colab.research.google.com/drive/1_1X9_N6ml0GIKEeVuv5qrC31L-wG24JP?usp=sharing)]

## Bibliografía

Speech and Language Processing - Dan Jurafsky,  capítulo 6 [[LINK](https://web.stanford.edu/~jurafsky/slp3/6.pdf)]

## **Videos**

**Estos videos son para uso exclusivo de los estudiantes de la materia.  No compartir**

- Clase Teórica I [[LINK](https://drive.google.com/file/d/1dsmy0Ls4WsanvamW7UPBMAoB8Q6Ev7ci/view?usp=sharing)]
- Clase Teórica II [[LINK](https://drive.google.com/file/d/1rYYu_0nRHwB15m17Y-5E9df-emIjjGCO/view?usp=sharing)]
- Clase Práctica I[[LINK](https://drive.google.com/file/d/11oL4-2iTfRVc9h4xFvXDh6KmXPx67FZi/view?usp=sharing)]
- Clase Práctica II [[LINK](https://drive.google.com/file/d/1Cv0yilXxQWtN0CjSr7liOyRE76zePP1B/view?usp=sharing)]
- [Presentación Clase](http://157.92.26.246/campus/mod/resource/view.php?id=602)
    
[http://157.92.26.246/campus/theme/image.php/moove/core/1660267509/f/pdf-24](http://157.92.26.246/campus/theme/image.php/moove/core/1660267509/f/pdf-24) 


## Apuntes

Repaso:
* Similaridad semántica -> Como se asocian las palabras?
* La semántica de una palabra se puede deducir de un contexto
* Term-context matrix -> Matriz que muestra como se asocian las palabras. Cuando co-ocurrieron en un el mismo contexto
* Similaridad semántica -> Tener en cuenta la frecuencia de las palabras. Entre dos pares, de pares de palabras, tiene más relevancia el par que involucra a la palabra con menos frecuencia. Porque si aparece poco, pero se repite mucho este par, es probable que haya más sentido ahí.
  * Si normalizo, también puedo ver la fuerza
* Identificar collocations -> palabras que aparecen juntas
  * Lista de colocations
  * Buscar bigramas en un corpus con un alto PPMI
* Term-context matrix -> Matriz de terminos y contexto. Se puede usar una ventana de tamaño (1,3,5,7,9)
* PMI - Co-ocurrencia normalizada

Word embeddings -> Represento una palabra en un espacio, y ahi puedo ver que tan cerca quedan dos palabras en ese espacio de representacion. Tomo un corpus muy grandes y ahí genero las asociaciones (siempre los embeddings se entrenan con algun corpus)

Métodos:
* SVD -> Matrices, y recortar dimensiones que no aportan valor
* Word2Vec (skip-gram) -> Red neuronal (TODO: Re check que paso)

Similitud coseno es una forma de sacar la distancia. cos(Alpha) = (v_1 * v_2) / (|v_1| . |v_2|) 

Una tarea que puede surgir, _word analogy task_. Buscar palabras sinónimos.


### Glove 

Global vector for word representations. 


### LSA vs Word2Vec

* Un paper llego a conclusión de que Word2Vec era mejor que LSA. Como? Probando ambos metodos para varias tareas muy diferentes (similaridad de palabras, la asociación más probable entre una palabra y un grupo, similitud entre palabras. Todos probados con varios dataset que existen para validar métodos). 
  * Word2Vec generaliza mejor que LSA
* Otro paper llegar  a que ese word2vec tenía algunos trucos para mejorarlo
  * Sliding windows con pesos según distancia (palabras más cercanas tienen más pesos)
  * Subsampleo de palabras frecuentes (se descartan con un trheshold)
  * Smoothing en el samples de los ejemplos negativos
* En corpus chicos, LSA funciona mejor (< 1e6)


### Pre-trained word-embeddings

* Word2vec
  * También está en gensim
  * Extraer collocations con cantidades mínimas, y frecuencia mínima en documentos (min_count=10, threshold=0.5)
  * PMI -> pointwise mutual information (PMI) // PNMI - Normalizado
* Glove
* Fastext
  * Parecido a word2vec. No solo palabras como tokens, tambien ngramas por letras.
  * De Facebook



TODO: Que es ejemplos negativo?


### Tf idf

* El rol de una palabra en una frase? Una palabra puede ser extra en una frase, y central en otra
* Hay metodos para calcular la centralidad de una valor. Word-extraction.
* Una forma sencilla -> Normalizar
  * Dividir al vector de frecuencia de palabras por la norma del vector.
* Term frequency weight
  * Las palabras que aparecen en mas documentos, es menos específica en ese documento. 
  * tf-idf
  * log ( 1+|D| / 1+|{d:t pertenece d}) + 1
  * Numero de documentos en el set de entrenamiento, sobre el numero de documentos en los que aparece el término y
  * Esos +1 son para evitar los 0.
* Lo que se suele hacer es:
  * tf-idf + normalización

Un problema normal con idf, tf, tf-idf: cuando tengo que calcular los pesos de un documento, las palabras del documento pero que no aparecen en mi dataset de entrenamiento, van a ser ignoradas. Un workaround es agregar un peso chiquito por default. 


### Bias y embeddings

Los modelos y los embeddings pueden estar sesgados, por que lo textos en los que son entrenados son sesgados. Los sesgos se pueden ver por las asociaciones de roles y generos, por las asociaciones de palabras entre dos conjuntos. 

Cada embedding tiene un corpus detrás, y puede estar entrenado con distintas variaciones. Que quiere decir, similaridad de 0.35 en un embedding puede ser altísima, y en otro muy bajo. También, similaridad es co-ocurrencia de palabras, que tanto apareció una palabra con otra en un mismo contexto. Los dataset tiene sus sesgos, y tienen su temáticas (corpus con los que se entrenaron), eso condiciona la asociación de palabras. 

Si entreno un embedding con reviews de películas, va a tener un set de palabras, un set de asociaciones muy distintos que si entreno con noticias, o con sueños, o con reviews de productos.

Cercano en los embeddings es que son palabras que van a aparecer en contextos muy similares.

## De la práctica


* Word2Vec -> Elegir bastante bien como preprocesar el texto. Trhesholds, n-gramas, limpieza, collocations, frecuencias de apareción.
  * Es estocástico, distintas ejecuciones, distintos resultados.
* LSA -> 
  * 1- Diccionario, un id para cada palabra
  * 2- filtrar frecuencias, y quedarse con un subset (keep_n)
    * Parte de la matriz de termino documento (td)
  * 3- corpus, 
    * Corpus -> Array de documentos, cada documento está representado como una lista de tuplas. Cada tupla es (id, frecuencia)
  * 4- td-idf
  * LdiModel con corpus, dictionary, num_topics
  * Es estocástico
* Pre-calculated embeddings
  * No necesariamente es bajarse el modelo, podria ser bajarse corpus grandes y específicos: Wikipedia, google news, ...


MDS y TSNE para graficar los embeddings. Capturan las distribuciones y las proyectan en un espacio de dimensionalidad menor. TSNE presta atención a las cercanías. MDS a lejanos y cercanos. 