# Languages Models (Práctica)

**[Languages Models (Práctica)](http://157.92.26.246/campus/course/view.php?id=13#section-11)**
    
## **Notebooks**

- Práctica de Modelos de Lenguaje basado en N-grams [[LINK](https://colab.research.google.com/drive/1UlunoIZBXkEztiBHmNS9GVSSYlvvcqPU?usp=sharing)]
- Práctica de Modelos de Lenguaje basado en RNN (para hacer en casa) [[LINK](https://colab.research.google.com/drive/1iVAPwvY408QLTHdgbu1fxnhyL4sShaL7?usp=sharing)]
- Práctica de NER basado en RNN [[LINK](https://colab.research.google.com/drive/1qtarO9gdt6z19qDJPZKrRJWWnBYv04q0?usp=sharing)]

## **Referencias**

Blog sobre sampling en Languge Models: [https://huggingface.co/blog/how-to-generate](https://huggingface.co/blog/how-to-generate)

## **Videos**

- Primera Parte [[LINK](https://drive.google.com/file/d/1X-1yImwM7fCLBYwUvZ0Ijz3Ser-8wvTU/view?usp=sharing)]
- Segunda Parte [[LINK](https://drive.google.com/file/d/1ySQp8QlPgM1CRb86JZ83tk6mK99iZyS7/view?usp=sharing)]

