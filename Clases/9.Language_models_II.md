# Languages Models II

**[Languages Models II](http://157.92.26.246/campus/course/view.php?id=13#section-10)**

## Recurrent Neural Networks

- Vanilla RNN
- LSTM
- GRU
- Bidirectional RNNs
- Multilayer RNNs
- Seq2seq models

## Bibliografía

Goodfellow, I. (2016). Bengio, Y.–Courville. A. Deep Learning. [[LINK](https://www.deeplearningbook.org/contents/rnn.html)]

## Videos

**Estos videos son para uso exclusivo de los estudiantes de la materia.  No compartir**

- Clase Teórica - Parte 1 [[LINK](https://drive.google.com/file/d/1rRbQ1170AzL8-BBpk1Ffz1mJxHZj6YdJ/view?usp=sharing)]
- Clase Teórica - Parte 2 [[LINK](https://drive.google.com/file/d/1okpXUUjXZN-tL4h2wPIIe--B-wr1nTyt/view?usp=sharing)]
- [PresentaciónFile](http://157.92.26.246/campus/mod/resource/view.php?id=316)
    
[http://157.92.26.246/campus/theme/image.php/moove/core/1660267509/f/pdf-24](http://157.92.26.246/campus/theme/image.php/moove/core/1660267509/f/pdf-24)


## Apuntes


Otra forma de pensar los modelos de lenguages es: _calcular la probabilidad de un texto_

Problema: ¿Cómo contamos cuántas veces aparece una palabra?

Hasta la clase pasada: 
* N-gram. Sacamos la probabilidad viendo las últimas n palabras (Eso simplifica el problema, y es bastante potente. Gastamos menos almacenamiento y es más fácil de procesar también)
  * Problema, no generaliza.
* Embeddings y Redes neuronales fast forward ayudan a generalizar.
  * Esquema: 
    * últimas n palabras
    * one-hot encoding
    * primera capa de la red, embeddings,
    * capa oculta
    * capa de salida, one-hot encoding
  * Problema, solo ven una parte del texto.
    * Hacerlas crecer es muy costoso. Si aumento más palalbras en el input, crecen los embeding, crecen las conexiones en las capas ocultas, todo se pone medio exponencial.
  * Sirven para aproximarnos al problema, pero no se utilizan.

El siguiente paso fue: **redes neuronales recurrentes**

* Arquitectura:
  * input
  * hidden layer
    * La capa oculta se alimenta a sí misma
  * output
* Lee una palabra a palabra. Es lo más común.
  * Pero podría ser caracter, sílabas, palabra, 2 palabras, ...
  * Uno define el token como quiera, por eso los tokenizadores. Ejemplo: Buenos Aires, probablemente quisiera que sean un token, no dos.
  * Funciona con series temporales, con cualquier otro problema.
* El largo de la secuencia es variable.
  * Voy una palabra a la vez, pero paso la secuencia que queria
* Matematicamente:
  * 3 matrices
    * U -> Multiplica el input
    * W -> Es la matriz que multiplica la salida de la palabra anterior
    * V -> Matriz que multiplica pre-salida 
    * palabra * U + salida_anterior * W
    * A eso por una tanh -> Ahí genero un output que multiplico por V, y un output que alimentará la proxima iteración.
  * Estas matrices se entrenan pero despues quedan fijas. 
  * Acá surge un problema con el calculo del gradiente para backpropagation
    * W y U dependen de lo que paso antes en la red recurrente.
    * Dos problemas:
      * Vanishing gradients -> Si las derivadas anteriores son muy chicas
        * El gradiente se va a 0. Se desvanece. Una opción es reducir el loop, ver menos palabras.
      * Exploding -> Si las derivadas son muy grandes
        * Gradient clipping es una opción para no diverger. Le pones como un techo al gradiente, eso hace que no te vayas tanto.
    * Por estos dos problemas, la red recurrente "vainilla" no puede capturar información de toda la frase.

No menor, pero redes neuronales son puro calculos matriciales. Que a su vez, en la forma de representar ecuaciones grandes.


### Gated recurrent units

La idea es sobrellevar el problema de retener la información de largo plazo (el problema que viene del modelo anterior). 

Incorpora compuertas (_gates_) para poder manejar mejor la actualización de los estados ocultos en cada iteración. 

Tanto el contenido de la actualización, como los gates, están determinados por el estado anterior y el input.

Incorpora dos gates:
* reset gate -> 
  * Vector producto de una sigmoidea (entre 0 y 1)
  * Me ayuda a filtrar la información anterior (deja pasar en los 1s, así filtra cierto ruido)
  * Este vector se aprende (es resultado de U y W)
* update gate
  * Vector producto de una sigmoidea (entre 0 y 1)
  * Me ayuda a filtrar h~, un subproducto
  * Me ayuda a filtrar el input del estado anterior por segunda vez
  * Este vector se aprende (es resultado de U y W)

Con estos dos gates se acuerda de parte de la información anterior. Los dos gates son el resultado de aplicar una sigmoidea a U y W, a distintos productos de matrices. Esas matrices se van aprendiendo durante las iteraciones. Esto hace que filtren la información que viene. 

Las compuertas podrían ayudar con los desvanecimientos, porque podrían no darle tanta atención a esa dimensión que va desvaneciendose y listo. Y para el exploding siempre podemos usar los máximos. 


### Long short-term memory

Son de 1997 a priori, pero recién se pudieron usar en los últimos años por la demanda de poder computo que tenían. 

Son parecidas a las GRU, tienen más compuertas y tienen un estado oculto llamado _cell state_. 

Estructura
* Ingreso de info, **Input gate**
* Egreso de info
  * Cell state (este es el nuevo, el que se suma)
  * Estado anterior
  * Salida para está palabra

El cell state es interesante, es un canal nuevo de información que arrastra más info. Interactua con dos variantes de _input_ + _estado previo_.

Es parecida GRU, pero con una compuerta más. Hay más matrices (8 matrices, vs 6 de GRU, + bias).


> Nunca está de más recordarlo, el modelo es una única cajita, y arrastra información. El modelo se alimenta a sí mismo con información anterior, y así crea memoria en las matrices. Pero el modelo es esa única cajita, esas n matrices. No cambia según el input del modelo, si tengo 5 palabras por frase, 3, 10, o 100 es lo mismo. Lo que si podría cambiar, que es otra charla, es la dimensionalidad del input y por lo tanto de las matrices.


### LSTM vs GRU

* GRU tiene menos parámetros, por lo tanto, más eficiente computacionalmente
* LSTM tiene más fama que GRU
* A priori no hay evidencia fuerte de que uno funcione mejor, y siempre es mejor testearlo
* En la práctica se suele usar LSTM y luego ver si GRU mejora
* Si tenemos un corpus en el que creemos que las dependencias de largo plazo son importantes, podría valer más la pena LSTM


### Usos

Hay muchos usos. Todo lo que tenga secuencialidad.

**Text generation**
* Texto -> Embedding -> Salida -> Soft max (distribución de probabilidad, asociado a un vector de one-hot encoding)
* Como elijo la próxima palabra:
  * La próxima palabra más probable
    * Hace algo medio cesgado, la mejor próxima palabra probablemente no genere la mejor oración resultante.
    * Se hace más determinístico. Una palabra, siempre va a generar una misma palabra.
  * La rama más probable (beam search)
    * Puedo elegir con ciertos espacios para adelante (dos palabras, cinco palabras, ...)
    * Una visión más general, pero problemas similares. Rutas "determinísticas"
    * Más grande es el look ahead, más costoso, pero se puede.
  * Sampleo
    * Agrego aleatoriedad, pero puedo terminar con oraciones muy poco probables, o con muy poco sentido.
  * Sampleo de las más probables
    * Para cada palabra, tomo una de las k más probables, eso ayuda a generar más sentido manteniendo cierta aleatoriedad.
    *  

* Cada método tiene sus pro-cons


**Clasificación**

1. Leo todo el texto, con el último output hago la clasificación. Tendría una distribución de probabilidades de output asociado a las clases que tengo.
2. Podría generar armar una capa, que se alimente con todos los outputs, y esa capa es la que me genera la distribución de probabilidades de output asociado a las clases que tengo.
   * Porque si bien las LSTM/GRU tienen información previa, se pierde. Podría querer usar todos los subproductos intermedios para mi tarea.
   * Esa capa sería un **sentence representation** ()



### Bidirectional RNNs

Dos LSTM que leen en ambas direcciones. Entonces captura yendo y viniendo. Los productos intermedios tienen contexto pasado y futuro. 

Esto no es un modelo de lenguaje porque tiene información futura. Pero puede ser útil para otras tareas que involucran la representación de una frase (clasificación, sentiment, resumenes). 


### Multi-layer RNNs

Apilo RNNs. Hay una matriz. 

 .-.-.-. (capa 3)
 |-|-|-|
 .-.-.-. (capa 2)
 |-|-|-|
 .-.-.-. (capa 1)
 |-|-|-|
(inputs)

Cada punto es una RNNs. De ancho no son varias, es una. La concatenación es la proyección en el tiempo, el procesamiento de tokens. 

El gran problema es el costo de entrenamiento. Por eso no se usan tanto.


### Seq2Seq model

El ejemplo más común es traducción. 

Encoder RNN -> Procesa todo el texto, y el estado oculto arranca una otra red **decoder RNN** (que es un modelo de lenguaje que se usa para generar texto)